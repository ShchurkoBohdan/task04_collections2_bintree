public class Node {
    private int nodeData;
    private Node left;
    private Node right;

    public Node(int data){
        this.nodeData = data;
        left = null;
        right = null;
    }

    public int getNodeData() {
        return nodeData;
    }

    public void setNodeData(int nodeData) {
        this.nodeData = nodeData;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}
