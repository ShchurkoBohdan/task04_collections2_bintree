public class Main {
    public static void main(String[] args) {
        BinaryTreeImplementation tree = new BinaryTreeImplementation();
        tree.insert(3);
        tree.insert(8);
        tree.insert(31);
        tree.insert(4);
        tree.insert(6);
        tree.insert(2);
        tree.insert(10);
        tree.insert(9);
        tree.insert(20);
        tree.insert(25);
        tree.insert(15);
        tree.insert(16);
        System.out.println("Original Tree : ");
        tree.display(tree.root);
        System.out.println("");
        System.out.println("Check whether Node with value 4 exists : " + tree.find(1));
        System.out.println("Delete Node with no children (2) : " + tree.delete(2));
        tree.display(BinaryTreeImplementation.root);
        System.out.println("\n Delete Node with one child (4) : " + tree.delete(4));
        tree.display(BinaryTreeImplementation.root);
        System.out.println("\n Delete Node with one child (54) : " + tree.delete(54));
        tree.display(BinaryTreeImplementation.root);
        System.out.println("\n Delete Node with Two children (10) : " + tree.delete(10));
        tree.display(BinaryTreeImplementation.root);
    }
}
